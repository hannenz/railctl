# Makefile from
# https://spin.atomicobject.com/2016/08/26/makefile-c-projects/
RASPBERRY_PI_HOST=pi@playground
PIGPIO_DIR=/pigpio-master

# CC=/opt/crosspi/arm-bcm2708/arm-rpi-4.9.3-linux-gnueabihf/bin/arm-linux-gnueabihf-g++
# CXX=/opt/crosspi/arm-bcm2708/arm-rpi-4.9.3-linux-gnueabihf/bin/arm-linux-gnueabihf-g++
CC=arm-linux-gnueabi-g++
CXX=arm-linux-gnueabi-g++

TARGET_EXEC ?= railctl

BUILD_DIR ?= build
SRC_DIRS ?= src
DATA_DIR ?= data

SRCS := $(shell find $(SRC_DIRS) -name *.cpp -or -name *.c -or -name *.s)
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o) $(PIGPIO_DIR)/libpigpio.so


INC_DIRS := $(shell find $(SRC_DIRS) -type d) $(PIGPIO_DIR)
INC_FLAGS := $(addprefix -I,$(INC_DIRS))
LDFLAGS := -lrt

CPPFLAGS ?= $(INC_FLAGS) -MMD -MP -Wall -g -O3

$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	$(CC) $(OBJS) -o $@ $(LDFLAGS)

# assembly
$(BUILD_DIR)/%.s.o: %.s
	$(MKDIR_P) $(dir $@)
	$(AS) $(ASFLAGS) -c $< -o $@

# c source
$(BUILD_DIR)/%.c.o: %.c
	$(MKDIR_P) $(dir $@)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

# c++ source
$(BUILD_DIR)/%.cpp.o: %.cpp
	$(MKDIR_P) $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@


.PHONY: clean deploy install

clean:
	$(RM) -r $(BUILD_DIR)

deploy:
	scp $(BUILD_DIR)/$(TARGET_EXEC) $(RASPBERRY_PI_HOST):

install:
	scp $(DATA_DIR)/railctl.service $(RASPBERRY_PI_HOST):/tmp/
	ssh $(RASPBERRY_PI_HOST) "sudo cp /tmp/loopo.service /etc/systemd/system/ && sudo systemctl enable loopo.service"


-include $(DEPS)

MKDIR_P ?= mkdir -p
