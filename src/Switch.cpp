#include <iostream>
#include "Switch.h"
#include "Led.h"
#include <pigpio.h>

using namespace std;



Switch::Switch(string name, State state, Current current, Led current_led, Led status_led1, Led status_led2, Led status_led3) {
	this->name = name;

	this->current_led = current_led;
	this->current_led.off();

	this->status_led1 = status_led1;
	this->status_led2 = status_led2;
	this->status_led3 = status_led3;

	this->status_led1.off();
	this->status_led2.off();
	this->status_led3.off();

	this->set_state(state);
	this->set_current(current);
}



Switch::~Switch() {
	this->current_led.off();
	this->status_led1.off();
	this->status_led2.off();
	this->status_led3.off();
}



Switch::State Switch::toggle() {
	cout << this->state << endl;
	if (this->state == STRAIGHT) {
		this->set_state(CURVED);
	}
	else {
		this->set_state(STRAIGHT);
	}
	return this->state;
}



Switch::State Switch::get_state() {
	return this->state;
}



Switch::State Switch::set_state(Switch::State state) {
	this->state = state;
	switch (this->state) {
		case STRAIGHT:
			cout << this->name << ": straight" << endl;
			this->status_led1.on();
			this->status_led2.off();
			this->status_led3.on();
			break;
		case CURVED:
			cout << this->name << ": curved" << endl;
			this->status_led1.off();
			this->status_led2.on();
			this->status_led3.on();
			break;
		default:
			cout << this->name << ": undefined state" << this->state << endl;
	}
	return this->state;
}



void Switch::set_name(string name) {
	this->name = name;
}



string Switch::get_name() {
	return this->name;
}

void Switch::test() {
	while (true) {
		cout << this->name << "Testing 1:\n";
		this->status_led1.on();
		this->status_led2.off();
		this->status_led3.off();
		gpioDelay(1 * 1000 * 1000);
		cout << this->name << "Testing 2:\n";
		this->status_led1.off();
		this->status_led2.on();
		this->status_led3.off();
		gpioDelay(1 * 1000 * 1000);
		cout << this->name << "Testing 3:\n";
		this->status_led1.off();
		this->status_led2.off();
		this->status_led3.on();
		gpioDelay(1 * 1000 * 1000);
	}
}


void Switch::set_current(Current current) {
	this->current = current;
	this->current_led.off();
	switch (this->current) {
		case RED:
			this->current_led.on(Led::RED);
			break;
		case GREEN:
			this->current_led.on(Led::GREEN);
			break;
		case BLUE:
			this->current_led.on(Led::BLUE);
			break;
	}
}



Switch::Current Switch::get_current() {
	return this->current;
}



void Switch::dump() {
	std::cout << "The switch \"" << this->name << "\" is " << ((this->state == STRAIGHT) ? "straight" : "curved") << ", current is " << this->current << std::endl;
}

