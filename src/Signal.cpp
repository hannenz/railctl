nn#include <iostream>
#include <unistd.h>
#include "Signal.h"
#include "Led.h"

using namespace std;


Signal::Signal(string name, State state, Led led) {
	this->name = name;
	this->led = led;
	this->set_state(state);
}


Signal::~Signal() {
	this->led.off();
}


Signal::State Signal::toggle() {
	if (this->get_state() == STOP) {
		this->set_state(GO);
	}
	else {
		this->set_state(STOP);
	}
	return this->get_state();
}


Signal::State Signal::get_state() {
	return this->state;
}


void Signal::set_state(Signal::State state) {
	this->state = state;
	Led::Color color;
	switch (this->state) {
		case GO:
			color = Led::GREEN;
			break;
		case STOP:
		default:
			color = Led::RED;
			break;
	}
		
	this->led.off();
	this->led.on(color);
}


void Signal::set_name(string name) {
	this->name = name;
}


string Signal::get_name() {
	return this->name;
}


void Signal::dump() {
	std::cout << "The signal \"" << this->name << "\" is " << ((this->state == GO) ? "<GO>" : "<STOP>") << std::endl;
}
