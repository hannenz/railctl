#include <iostream>
#include <csignal>
#include <cstdlib>
#include <unistd.h>
#include <pigpio.h>
#include "Switch.h"
#include "Signal.h"
#include "Led.h"
#include "Controller.h"

using namespace std;

Controller *ctl;

void clean_up() {
	cout << "Cleaning up, terminating.\n";
	delete ctl;
	cout << "Terminating GPIO\n";
	gpioTerminate();
}


void signalHandler(int signum) {
	exit(signum);
}


int main() {

	if (gpioInitialise() < 0) {
		cerr << "Failed to initialise gpio" << endl;
		return -1;
	}

	atexit(clean_up);
	
	// Install our signal handlers
	signal(SIGINT, signalHandler); 	// (2) Ctrl-C
	signal(SIGTERM, signalHandler); // (14) Shutdown etc.

	cout << "*** It really works (cross compiled via docker!) ***" << endl;

	ctl = new Controller();
	ctl->run();

	// gpioSetMode(12, PI_INPUT);
	// while (1) {
	// 	if (gpioRead(12)) {
	// 		cout << "TRUE: GREEN" << endl;
	// 	}
	// 	else {
	// 		cout << "FALSE: RED" << endl;
	// 	}
	// 	gpioDelay(1000);
	// }
	//

	//clean_up();
	return 0;
}
