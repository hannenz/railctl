#include <iostream>
#include <pigpio.h>
#include <cstdlib>
#include "Led.h"

using namespace std;

Led::Led()
{
	// Intentionally left blank
}

Led::Led(int pin)
{
	this->_pins[0] = pin;
	gpioSetMode(this->_pins[0], PI_OUTPUT);
	this->status = false;
	gpioWrite(this->_pins[0], this->status);
	this->blinkCount = 0;
	this->colors = 1;
}


/**
 * Bi-Color (red/green)
 */
Led::Led(int pin_red, int pin_green)
{
	this->_pins[0] = pin_red;
	this->_pins[1] = pin_green;
	gpioSetMode(this->_pins[0], PI_OUTPUT);
	gpioSetMode(this->_pins[1], PI_OUTPUT);
	gpioWrite(this->_pins[0], 0);
	gpioWrite(this->_pins[1], 0);
	this->status = false;
	this->blinkCount = 0;
	this->colors = 2;
}


/**
 * RGB-LED (red, green, blue)
 */
Led::Led(int pin_red, int pin_green, int pin_blue)
{
	this->_pins[0] = pin_red;
	this->_pins[1] = pin_green;
	this->_pins[2] = pin_blue;
	gpioSetMode(this->_pins[0], PI_OUTPUT);
	gpioSetMode(this->_pins[1], PI_OUTPUT);
	gpioSetMode(this->_pins[2], PI_OUTPUT);
	gpioWrite(this->_pins[0], 0);
	gpioWrite(this->_pins[1], 0);
	gpioWrite(this->_pins[2], 0);
	this->status = false;
	this->blinkCount = 0;
	this->colors = 3;
}




Led::~Led() 
{
	//
}



void Led::on()
{
	this->status = true;
	gpioWrite(this->_pins[0], this->status);
}


void Led::off()
{
	this->status = false;
	for (int i = 0; i < this->colors; i++) {
		gpioWrite(this->_pins[i], this->status);
	}
}


void Led::on(Led::Color color)
{
	this->status = color;
	gpioWrite(this->_pins[color], true);
}


void Led::toggle()
{
	this->status = !this->status;
	gpioWrite(this->_pin, this->status);
}

void Led::toggle(Led::Color color)
{
	int status = gpioRead(this->_pins[color]);
	gpioWrite(this->_pins[color], !status);
}


bool Led::getStatus() {
	return this->status;
}

void Led::blink(int delay_ms, int count)
{
	this->blinkCount = count * 2;
	while (this->blinkCount-- > 0) {
		this->toggle();
		gpioDelay(delay_ms * 1000);
	}
}


void Led::blink(int delay_ms, int count, Led::Color color) 
{
	this->blinkCount = count * 2;
	while (this->blinkCount-- > 0) {
		this->toggle(color);
		gpioDelay(delay_ms * 1000);
	}
}


void Led::blinkBg(int delay_ms, int count) 
{
	this->blinkFreq = delay_ms;
	this->blinkCount = count * 2;

	gpioSetTimerFuncEx(BLINK_TIMER_NR, this->blinkFreq, this->blinkCb, (void*)this);
}


void Led::blinkCb(void *user_data) {
	Led *myself = (Led*)user_data;
	cout << "Blink count: " << myself->blinkCount << endl;
	if (--myself->blinkCount <= 0)
	{
		myself->blinkCount = 0;
		cout << "Stop blinking!" << endl;
		gpioSetTimerFuncEx(BLINK_TIMER_NR, myself->blinkFreq, NULL, NULL);
	}
	myself->toggle();
}


