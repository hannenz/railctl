#ifndef __SIGNAL_H__
#define __SIGNAL_H__
#include <string>
#include "Led.h"

using std::string;

class Signal {

	public:
		enum State {
			STOP,
			GO
		}; 

		Signal(string name, State state, Led led);
		~Signal();
		State toggle();
		State get_state();
		void set_state(State state);
		void set_name(string name);
		string get_name();
		void dump();

	private:
		State state;
		string name;
		Led led;

};

#endif
