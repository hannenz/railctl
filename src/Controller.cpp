#include "Controller.h"
#include "Signal.h"
#include "Switch.h"
#include <unistd.h>
#include <cstdlib>

using namespace std;



/**
 * Table of all states
 * as determined by Lorenz
 */
const Controller::State Controller::states[] = {
	{ Switch::STRAIGHT, Switch::STRAIGHT, Signal::GO, Signal::STOP, Signal::STOP, Switch::RED, Switch::RED },
	{ Switch::STRAIGHT, Switch::STRAIGHT, Signal::STOP, Signal::GO, Signal::GO, Switch::BLUE, Switch::GREEN },
	{ Switch::STRAIGHT, Switch::STRAIGHT, Signal::STOP, Signal::GO, Signal::STOP, Switch::BLUE, Switch::GREEN },
	{ Switch::STRAIGHT, Switch::STRAIGHT, Signal::STOP, Signal::STOP, Signal::GO, Switch::BLUE, Switch::GREEN },
	{ Switch::STRAIGHT, Switch::STRAIGHT, Signal::STOP, Signal::STOP, Signal::STOP, Switch::BLUE, Switch::GREEN },

	{ Switch::STRAIGHT, Switch::CURVED, Signal::GO, Signal::STOP, Signal::STOP, Switch::GREEN, Switch::GREEN },
	{ Switch::STRAIGHT, Switch::CURVED, Signal::STOP, Signal::GO, Signal::GO, Switch::BLUE, Switch::RED },
	{ Switch::STRAIGHT, Switch::CURVED, Signal::STOP, Signal::GO, Signal::STOP, Switch::BLUE, Switch::RED },
	{ Switch::STRAIGHT, Switch::CURVED, Signal::STOP, Signal::STOP, Signal::GO, Switch::BLUE, Switch::RED },
	{ Switch::STRAIGHT, Switch::CURVED, Signal::STOP, Signal::STOP, Signal::STOP, Switch::BLUE, Switch::RED },

	{ Switch::CURVED, Switch::STRAIGHT, Signal::GO, Signal::STOP, Signal::GO, Switch::BLUE, Switch::GREEN },
	{ Switch::CURVED, Switch::STRAIGHT, Signal::GO, Signal::STOP, Signal::STOP, Switch::BLUE, Switch::GREEN },
	{ Switch::CURVED, Switch::STRAIGHT, Signal::STOP, Signal::GO, Signal::STOP, Switch::RED, Switch::RED },
	{ Switch::CURVED, Switch::STRAIGHT, Signal::STOP, Signal::STOP, Signal::GO, Switch::BLUE, Switch::GREEN },
	{ Switch::CURVED, Switch::STRAIGHT, Signal::STOP, Signal::STOP, Signal::STOP, Switch::BLUE, Switch::GREEN },

	{ Switch::CURVED, Switch::CURVED, Signal::GO, Signal::STOP, Signal::GO, Switch::BLUE, Switch::RED },
	{ Switch::CURVED, Switch::CURVED, Signal::GO, Signal::STOP, Signal::STOP, Switch::BLUE, Switch::RED },
	{ Switch::CURVED, Switch::CURVED, Signal::STOP, Signal::GO, Signal::STOP, Switch::GREEN, Switch::GREEN },
	{ Switch::CURVED, Switch::CURVED, Signal::STOP, Signal::STOP, Signal::GO, Switch::BLUE, Switch::RED },
	{ Switch::CURVED, Switch::CURVED, Signal::STOP, Signal::STOP, Signal::STOP, Switch::BLUE, Switch::RED }
};


void Controller::on_btn_pressed(int pin, int level, uint32_t tick, void *user_data) {
	static uint32_t t0, dur_ms;
	Controller *self = (Controller*)user_data;
	cout << "Button pressed: " << pin << ", Level: " << level << endl;

	// Debounce
	if (!level) {
		t0 = tick;
		return;
	}
	else {
		dur_ms = (tick - t0) / 1000;
		t0 = 0;
		cout << "Duration: " << dur_ms << " ms" << endl;
		if (dur_ms < 50) {
			return;
		}
	}

	switch (pin) {
		case SIGNAL_1_BTN:
			self->signal1->toggle();
			break;
		case SIGNAL_2_BTN:
			self->signal2->toggle();
			break;
		case SIGNAL_3_BTN:
			self->signal3->toggle();
			break;
		case SWITCH_1_BTN:
			self->dkw2->toggle();
			break;
		case SWITCH_2_BTN:
			self->dkw1->toggle();
			break;
		default:
			cout << "Unknown button pressed" << endl;
			break;
	}

	self->update();
}


/**
 * Constructor
 * Set up 2 DKW (Doppelkreuzweichen) switches and the 3 signals
 */
Controller::Controller() {
	dkw2 = new Switch("DKW 2", Switch::STRAIGHT, Switch::BLUE, Led(26, 19, 13), Led(16), Led(20), Led(21));
	dkw1 = new Switch("DKW 1", Switch::STRAIGHT, Switch::GREEN, Led(11, 9, 10), Led(1), Led(8), Led(7));

	signal1 = new Signal("Signal 1", Signal::STOP, Led(24, 23));
	signal2 = new Signal("Signal 2", Signal::STOP, Led(27, 22));
	signal3 = new Signal("Signal 3", Signal::STOP, Led(4, 17));

	// Setup buttons
	gpioSetMode(SIGNAL_3_BTN, PI_INPUT);
	gpioSetMode(SIGNAL_2_BTN, PI_INPUT);
	gpioSetMode(SIGNAL_1_BTN, PI_INPUT);
	gpioSetMode(SWITCH_1_BTN, PI_INPUT);
	gpioSetMode(SWITCH_1_BTN, PI_INPUT);
	gpioSetPullUpDown(SIGNAL_3_BTN, PI_PUD_UP);
	gpioSetPullUpDown(SIGNAL_2_BTN, PI_PUD_UP);
	gpioSetPullUpDown(SIGNAL_1_BTN, PI_PUD_UP);
	gpioSetPullUpDown(SWITCH_1_BTN, PI_PUD_UP);
	gpioSetPullUpDown(SWITCH_2_BTN, PI_PUD_UP);
	gpioSetAlertFuncEx(SIGNAL_3_BTN, on_btn_pressed, this);
	gpioSetAlertFuncEx(SIGNAL_2_BTN, on_btn_pressed, this);
	gpioSetAlertFuncEx(SIGNAL_1_BTN, on_btn_pressed, this);
	gpioSetAlertFuncEx(SWITCH_1_BTN, on_btn_pressed, this);
	gpioSetAlertFuncEx(SWITCH_2_BTN, on_btn_pressed, this);
}



/**
 * Destructor - Cleaning up
 */
Controller::~Controller() {
	delete signal1;
	delete signal2;
	delete signal3;
	delete dkw1;
	delete dkw2;
}



/**
 * Main loop
 */
void Controller::run() {

	char c = '\0';
	while (c != 'q') {
		cin >> c;
		switch (c) {

			case '1':
				signal1->toggle();
				update();
				break;

			case '2':
				signal2->toggle();
				update();
				break;

			case '3':
				signal3->toggle();
				update();
				break;

			case 'a':
				dkw1->toggle();
				update();
				break;

			case 'b':
				dkw2->toggle();
				update();
				break;

			default:
				cout << "?";
				break;
		}
		cout << endl;
	}
}



/**
 * Compare two states and calculate the differences between them
 *
 * @param Controller::State state1 		The first state to be compared
 * @param Controller::State state2		The second state to be compared
 * @return int 							Nr. of differences
 */
int Controller::compare_states(State state1, State state2) {
	int n = 0;
	if (state1.switch1_state != state2.switch1_state) {
		n++;
	}
	if (state1.switch2_state != state2.switch2_state) {
		n++;
	}
	if (state1.signal1_state != state2.signal1_state) {
		n++;
	}
	if (state1.signal2_state != state2.signal2_state) {
		n++;
	}
	if (state1.signal3_state != state2.signal3_state) {
		n++;
	}
	return n;
}


/**
 * Update: Look up the desired (new) state in the states table and switch to the
 * one with the least differences, which in the best case is the originating new
 * state again, so that the state is practically "confirmed". Else we switch to
 * the state with the least differences compared to the desired state
 *
 * @return void
 */
void Controller::update() {
	unsigned int i, j, len;
	unsigned int min_differences = 0;
	unsigned int differences;

	this->signal1->dump();
	this->signal2->dump();
	this->signal3->dump();
	this->dkw1->dump();
	this->dkw2->dump();

	len = sizeof(this->states) / sizeof(State);

	State current_state;
	current_state.switch1_state = dkw1->get_state();
	current_state.switch2_state = dkw2->get_state();
	current_state.signal1_state = signal1->get_state();
	current_state.signal2_state = signal2->get_state();
	current_state.signal3_state = signal3->get_state();
	// current_state.switch1_current = dkw1->get_current();
	// current_state.switch2_current = dkw2->get_current();

	j = 0;
	for (i = 0; i < len; i++) {
		differences = compare_states(current_state, this->states[i]);
		if (differences <= min_differences) {
			min_differences = differences;
			j = i;
		}
	}

	// j holds the state with the least differences compared to the desired
	// state (0 differences = the state that has been requested has been found
	// and is ok, no change...

	// Apply state[j] and call it a day .. ?!?
	dkw1->set_state(this->states[j].switch1_state);
	dkw1->set_current(this->states[j].switch1_current);
	dkw2->set_state(this->states[j].switch2_state);
	dkw2->set_current(this->states[j].switch2_current);

	signal1->set_state(this->states[j].signal1_state);
	signal2->set_state(this->states[j].signal2_state);
	signal3->set_state(this->states[j].signal3_state);

	return;
}

