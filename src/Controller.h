#include <iostream>
#include <pigpio.h>
#include "Switch.h"
#include "Signal.h"
#include "Led.h"


#define SIGNAL_1_BTN 6
#define SIGNAL_2_BTN 5
#define SIGNAL_3_BTN 12
#define SWITCH_1_BTN 14
#define SWITCH_2_BTN 15


class Controller {

	public:
		struct State {
			Switch::State switch1_state;
			Switch::State switch2_state;
			Signal::State signal1_state;
			Signal::State signal2_state;
			Signal::State signal3_state;
			Switch::Current switch1_current;
			Switch::Current switch2_current;
		};
		Signal *signal1, *signal2, *signal3;
		Switch *dkw1, *dkw2;

		Controller();
		~Controller();
		void run();
	
	private:
		State current_state;

		static void on_btn_pressed(int pin, int level, uint32_t tick, void *user_data);

		int compare_states(State state1, State state2);
		void update();

		static const State states[];
};
