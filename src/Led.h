#ifndef __LED_H__
#define __LED_H__

#define BLINK_TIMER_NR 1


class Led {
	private:
		int _pin;
		int _pins[3];
		int status;
		int blinkFreq;
		int blinkCount;
		static void blinkCb(void *userData);
		int colors;

	public:
		enum Color {
			RED,
			GREEN,
			BLUE
		};

		Led();
		Led(int pin);
		Led(int pin_red, int pin_green);
		Led(int pin_red, int pin_green, int pin_blue);
		~Led();

		void on();
		void on(Led::Color color);
		void off();
		void toggle();
		void toggle(Led::Color color);
		void blink(int freq, int count);
		void blink(int freq, int count, Led::Color color);
		void blinkBg(int freq, int ocunt);
		bool getStatus();
};

#endif
