#ifndef __SWITCH_H__
#define __SWITCH_H__
#include <string>
#include "Led.h"

using std::string;

class Switch {

	public:
		enum State {
			STRAIGHT,
			CURVED
		}; 

		enum Current {
			RED,
			BLUE,
			GREEN
		};


		Switch(string name, State state, Current current, Led current_led, Led status_led1, Led status_led2, Led status_led3);
		~Switch();
		State toggle();
		State get_state();
		State set_state(State state);
		void set_name(string name);
		Current get_current();
		void set_current(Current current);
		string get_name();
		void dump();
		void test();

	private:
		State state;
		string name;
		Current current;
		Led current_led;
		Led status_led1;
		Led status_led2;
		Led status_led3;
};
#endif
